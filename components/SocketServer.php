<?php

namespace app\components;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

/**
 * Class SocketServer
 * @package app\components
 * To start server type:
 * php yii socket/start-socket
 * stop by Ctrl+C
 * or:
 * php yii socket/start-socket &
 *
 * To stop by pid:
 * sudo netstat -tulpn| grep :8080
 * kill -2339 PID
 */
class SocketServer implements MessageComponentInterface
{
    protected $clients;
    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        echo $msg . "\n";

        foreach ($this->clients as $client) {

            $client->send($msg);
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}