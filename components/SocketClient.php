<?php

namespace app\components;

use Yii;
use WebSocket\Client;
use WebSocket\ConnectionException;

class SocketClient
{

    public function send($msg)
    {
        $host = Yii::$app->params['socketHost'];

        try {
            (new Client('ws://' . $host . ':8080/'))->send($msg);

        } catch (ConnectionException $e) {
            Yii::error('Cant connect to socket server. ' . $e->getMessage(), 'system');
        }
    }
}