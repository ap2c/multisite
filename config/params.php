<?php

return [
    'adminEmail' => 'admin@example.com',
    'socketHost' => YII_ENV_DEV ? 'multisite.loc' : 'multisite.ddns.net',
];
