<?php
$host = Yii::$app->params['socketHost'];
?>

<h1>Dashboard</h1>

<div class="index">


    <table class="table table-bordered">
        <thead>
            <tr>
                <th>datetime</th>
                <th>type</th>
                <th>author</th>
                <th>changed fields</th>
            </tr>
        </thead>
        <tbody id="events">
            <?php foreach($events as $e): ?>
                <tr>
                    <td><?=$e->created_at ?></td>
                    <td><?=$e->type ?></td>
                    <td><?=$e->author ?></td>
                    <td><?=$e->change ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">

    socket = new WebSocket('ws://<?=$host?>:8080');
    socket.onopen = function(e) {
        $('#events').prepend('<tr><td colspan="9">&nbsp;</td></tr>');
    };
    socket.onmessage = function(e) {
        console.log(e.data);
        var item = $.parseJSON(e.data);
        var labelColors = {'read': 'info', 'update': 'warning', 'insert': 'warning', 'delete': 'danger'};
        var itemType = '<span class="label label-' + labelColors[item.type] + '">' + item.type + '</span>';

        $('#events').prepend('<tr><td>'+item.created_at+'</td><td>'+itemType+'</td><td>'+item.author+'</td><td>'+item.change+'</td></tr>');
    };
</script>