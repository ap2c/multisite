<?php

namespace app\models;

use Yii;
use app\models\Event;
use app\components\SocketClient;

/**
 * This is the model class for table "client".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property integer $age
 * @property string $birthday
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'surname', 'email', 'age', 'birthday'], 'safe'],
            [['name', 'surname', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'email' => 'Email',
            'age' => 'Age',
            'birthday' => 'Birthday',
        ];
    }

    /**
     * @inheritdoc
     * @return ClientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClientQuery(get_called_class());
    }


    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $msg = [
            'type' => $insert ? 'insert' : 'update',
            'author' => Yii::$app->user->identity->username,
            'change' => $insert ? '' : implode(', ', array_keys($changedAttributes)),
            'created_at' => date('Y-m-d H:i:s'),
        ];

        (new SocketClient())->send(json_encode($msg));
    }

    public function afterDelete()
    {
        parent::afterDelete();

        $msg = [
            'type' => 'delete',
            'author' => Yii::$app->user->identity->username,
            'change' => '',
            'created_at' => date('Y-m-d H:i:s'),
        ];

        (new SocketClient())->send(json_encode($msg));
    }


}
